<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DashboardController extends Controller
{
    public function index()
    {
        try {
            $access_token = session()->get('access_token');
            $sub = session()->get('sub');
            $application = session()->get('application');

            $response = Http::asForm()
                ->withToken($access_token)
                ->post(config('sso.uri_auth') . '/realms/SPBE/protocol/openid-connect/token', [
                    'grant_type' => 'client_credentials',
                    'client_id' => config('sso.client_id'),
                    'client_secret' => config('sso.client_secret')
                ]);

            if ($response->status() != 200)
                throw new HttpException(Response::HTTP_BAD_REQUEST, $response->json()['error_description']);

            $data = $response->json();
            $access_token = $data['access_token'];

//            $role_mappings = Http::acceptJson()
//                ->withToken($access_token)
//                ->get(config('sso.uri_auth') . '/admin/realms/SPBE/users/' . $sub . '/role-mappings');

            $client_mappings = Http::acceptJson()
                ->withToken($access_token)
                ->get(config('sso.uri_auth') . '/admin/realms/SPBE/clients');

//            return session()->all();

//            return $client_mappings->json();

            if ($client_mappings->status() != 200)
                throw new HttpException(Response::HTTP_BAD_REQUEST, $client_mappings->json()['error_description']);

            $client_mappings = $client_mappings->json();

            $applications = [];
            foreach ($application as $key => $value) {
                foreach ($client_mappings as $client_mapping) {
                    if ($client_mapping['clientId'] == $key) {
                        if (!in_array($client_mapping['clientId'], ['account', 'realm-management', config('sso.client_id')])) {
                            $redirectUri = str_replace("*", "", $client_mapping['redirectUris']);
                            $redirectUri = count($redirectUri) > 0 ? $redirectUri[0] : $client_mapping['rootUrl'];

                            if (substr($redirectUri, -1))
                                $redirectUri = substr($redirectUri, 0, (strlen($redirectUri) - 1));

                            $applications[] = [
                                'name' => isset($client_mapping['name']) ? $client_mapping['name'] : $client_mapping['clientId'],
                                'clientId' => $client_mapping['clientId'],
                                'uri' => $redirectUri,
                            ];
                        }
                    }
                }
            }

            return view('dashboard', [
                'applications' => $applications
            ]);
        } catch (HttpException $exception) {
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                    'code' => $exception->getStatusCode()
                ]
            );
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode()
                ]
            );
        }
    }

    function search($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

//            foreach ($array as $subarray) {
//                $results = array_merge($results, $this->search($subarray, $key, $value));
//            }
        }

        return $results;
    }
}
