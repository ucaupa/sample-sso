<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo/>
        </x-slot>

        <x-jet-validation-errors class="mb-4"/>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            <div class="mt-4 px-3 pt-10">
                <p>Selamat Datang di Portal</p>
            </div>
            <div class="flex items-center pb-10 pt-4">
                <a href="{{ config('sso.uri_auth') . '/realms/SPBE/protocol/openid-connect/auth?client_id=' . config('sso.client_id') . '&state=&redirect_uri=' . config('sso.redirect') . '&response_type=code' }}"
                   class="w-full ml-2 inline-flex items-center h-8 px-4 ml-2 text-sm text-red-100 transition-colors duration-150 bg-red-700 rounded-lg focus:shadow-outline hover:bg-indigo-800 bg-red-400">
                    {{ __('Masuk SSO') }}
                </a>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
