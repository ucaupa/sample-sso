<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form action="{{ route('update_user', $id) }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <input type="text" name="username" value="{{ $user->username }}">
                    <input type="text" name="rule" value="{{ $user->rule }}">
                    <button type="submit">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
