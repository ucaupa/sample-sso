<?php

use App\Http\Controllers\Auth\SSOController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sso/callback', [SSOController::class, 'callback']);

Route::middleware(['auth:sanctum', 'verified', 'sso'])->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/user/{id}/edit', [SSOController::class, 'edit_user']);
    Route::patch('/user/{id}/edit', [SSOController::class, 'update_user'])->name('update_user');
});
