<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class user_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'email' => 'satu@mail.com',
                'name' => 'User Satu',
                'username' => 'satu',
                'password' => bcrypt('12345678'),
                'rule' => 'admin',
            ],
        ];

        DB::table('users')->insert($data);
    }
}
