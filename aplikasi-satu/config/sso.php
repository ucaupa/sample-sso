<?php

return [

    'uri' => env('SSO_URI', 'https://sso.layanan.go.id'),
    'uri_auth' => env('SSO_URI', 'https://sso.layanan.go.id') . '/auth',
    'redirect' => env('SSO_CLIENT_REDIRECT_URI') . '/sso/callback',
    'client_id' => env('SSO_CLIENT_ID'),
    'client_secret' => env('SSO_CLIENT_SECRET')

];
