<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-4">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                    <div class="mt-8 text-2xl">
                        Welcome to <strong>Aplikasi Satu</strong>!
                    </div>

                    <div class="mt-6">
                        <div class="mb-4">
                            <strong>Roles:</strong>
                            <div class="mt5">
                                <div class="my-2 flex items-center">
                                    <ul class="list-disc list-inside">
                                        @foreach(session()->get('roles') as $value)
                                            <li>{{$value}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="mb-4">
                            <strong>Akses Token</strong>
                            <div class="mt5">
                                <p>{{session()->get('access_token')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <x-jet-welcome/>
            </div>
        </div>
    </div>
</x-app-layout>
