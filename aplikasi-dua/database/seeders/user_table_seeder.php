<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class user_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'email' => 'satu@mail.com',
                'name' => 'User Dua',
                'username' => 'user_dua',
                'password' => bcrypt('12345678'),
                'role' => 'admin',
            ],
        ];

        DB::table('users')->insert($data);
    }
}
