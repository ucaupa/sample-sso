<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SSOController extends Controller
{
    public function callback(Request $request)
    {
        try {
            $code = $request->code;

            $response = Http::asForm()
                ->withBasicAuth(config('sso.client_id'), config('sso.client_secret'))
                ->post(config('sso.uri_auth') . '/realms/SPBE/protocol/openid-connect/token', [
                    'grant_type' => 'authorization_code',
                    'code' => $code,
                    'client_id' => config('sso.client_id'),
                    'client_secret' => config('sso.client_secret'),
                    'redirect_uri' => config('sso.redirect'),
                ]);

            if ($response->status() != 200)
                throw new HttpException(Response::HTTP_BAD_REQUEST, $response->json()['error_description']);

            $data = $response->json();

            $access_token = $data['access_token'];
            $token_decode = $this->accessTokenDecode($access_token);

            session([
                'access_token' => $access_token,
                'roles' => isset($token_decode['role_app']['roles']) ? $token_decode['role_app']['roles'] : [],
                'application' => $token_decode['resource_access'],
            ]);

            $user_sso = Http::asForm()
                ->withToken($access_token)
                ->get(config('sso.uri_auth') . '/realms/SPBE/protocol/openid-connect/userinfo');

            if ($user_sso->status() != 200)
                throw new HttpException(Response::HTTP_FORBIDDEN, $user_sso->json()['error_description']);

            $user = User::query()
                ->where('username', $user_sso['preferred_username'])
                ->first();

            if (empty($user)) {
                $user = new User();
                $user->name = $user_sso['name'];
                $user->username = $user_sso['preferred_username'];
                $user->email = $user_sso['email'];
                $user->password = bcrypt('12345678');
                $user->created_from = config('sso.uri_auth');
                $user->role = 'user';
                $user->save();
            }

            $user = User::query()
                ->where('username', $user_sso['preferred_username'])
                ->first();

            if (Auth::login($user)) {
                $request->session()->regenerate();

                return redirect()->intended('dashboard');
            }

            return redirect()->intended('dashboard');
        } catch (HttpException $exception) {
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                    'code' => $exception->getStatusCode()
                ]
            );
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                    'code' => $exception->getCode()
                ]
            );
        }
    }

    public function edit_user($id)
    {
        $user = User::query()->where('id', $id)->first();
        return view('user.edit', [
            'user' => $user,
            'id' => $id
        ]);
    }

    public function update_user($id, Request $request)
    {
        $user = User::query()->where('id', $id)->first();

        if (Auth::user()->rule == 'admin') {
            $user->rule = $request->rule;
            $user->update();
        }

        return redirect()->back();
    }

    public function accessTokenDecode($token)
    {
        $explode = explode('.', $token);
        $data = json_decode(base64_decode($explode[1]), true);

        return [
            'sub' => isset($data['sub']) ? $data['sub'] : null,
            'azp' => isset($data['azp']) ? $data['azp'] : null,
            'role_app' => isset($data['resource_access'][$data['azp']]) ? $data['resource_access'][$data['azp']] : [],
            'realm_access' => isset($data['realm_access']) ? $data['realm_access'] : [],
            'resource_access' => isset($data['resource_access']) ? $data['resource_access'] : [],
            'scope' => isset($data['scope']) ? $data['scope'] : null,
        ];
    }
}
